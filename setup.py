from distutils.core import setup

setup(
    name='getdoi',
    version='0.1.0',
    author='Tim Burke',
    author_email='timburke@stanford.edu',
    packages=['getdoi'],
    package_data={'getdoi': ['templates/*.tpl']},
    description='Convenient way to programmatically get metadata like journal, abstract and title from a DOI.'
)