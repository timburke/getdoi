#abstract.py
# A web bassed scraper designed to get the abstract from a DOI
#
# Currently supports scraping the following journal sites
# 

import lookup
import urllib2
from memoize import memoized
from bs4 import BeautifulSoup

def soupify_url(url):
	
	try:
		file = urllib2.urlopen(url)
	except urllib2.URLError:
		return {"error": "URL Error"}
			
	return BeautifulSoup(file)

#Journal website scrapers
def scrape_afm(doi, **kw):
	metadata = lookup.get_metadata(doi) #recall since we're memoized anyway
	
	soup = soupify_url(metadata['article']['url'])
	
	parent_div = soup.find(id='graphicalAbstract')
	abs = parent_div.find("div", {"class": "para"})
	
	if "verbose" in kw:
		print parent_div
		print "***"
		print abs
	
	abstract_strings = abs.contents[0].stripped_strings
	
	abstract = "".join(abstract_strings)
	
	return {'abstract': abstract}

def scrape_prl(doi, **kw):
	metadata = lookup.get_metadata(doi) #recall since we're memoized anyway
	
	soup = soupify_url(metadata['article']['url'])
	abstract = "".join(soup.find_all('div', {'class', 'aps-abstractbox'})[0].contents[0].stripped_strings)
	
	return {'abstract': abstract}

#register all the scrapers we have available
journal_handlers = {}
journal_handlers[u'Advanced Functional Materials'] = scrape_afm
journal_handlers[u'Physical Review Letters'] = scrape_prl

@memoized
def get_abstract(doi, **kw):
	global journal_handlers 
	
	metadata = lookup.get_metadata(doi)
	
	journal = metadata['journal']['title']
	
	if journal in journal_handlers:
		return journal_handlers[journal](doi, **kw)

	#We could not handle this journal (yet), return nothing
	return {}