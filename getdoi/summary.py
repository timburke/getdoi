#!/usr/bin/python
#Takes in a list of DOIs, looks them up and organizes the results

import lookup, abstract
import tempfile, string
import os, os.path
from mako.template import Template
from mako.lookup import TemplateLookup

#TODO: Add error handling if we can't find the DOI
def fetch_dois(dois, silent=False, **kw):
	results = []
	errors = []

	for doi in dois:
		if not silent:
			print "Looking up %s" % doi
			
		meta = lookup.get_metadata(doi)
		abs = abstract.get_abstract(doi, **kw)
		
		meta["article"].update(abs)
		
		if "notes" in kw:
			if doi in kw["notes"]:
				meta["article"]["notes"] = kw["notes"][doi]
		if "audience" in kw:
			if doi in kw["audience"]:
				meta["article"]["audience"] = kw["audience"][doi]
		
		results.append(meta)
	
	return (results, errors)
	
def organize_dois(dois, **kw):
	(results, errors) = fetch_dois(dois, **kw)
	
	organized = {}
		
	for result in results:
		journal = result["journal"]["title"]
		
		if journal in organized:
			organized[journal].append(result)
		else:
			organized[journal] = [result]
	
	return organized
	
def print_dois(dois):
	results = organize_dois(dois)
		
	for journal in results.values():
		print journal[0]["journal"]["title"]
		
		for article in journal:
			print article["article"]["title"]
			
def template_dir():
	return os.path.join(os.path.dirname(__file__), "templates")
			
def render_dois(dois, template_file, **kw):
	results = organize_dois(dois, **kw)
	
	lookup_dirs = ['.', template_dir(), '/']
	
	lookup = TemplateLookup(directories = lookup_dirs)
	
	template = lookup.get_template(template_file)
	
	return template.render_unicode(data=results)
	
def load_dois_from_file(filename):
	file = open(filename,'r')
	
	notes = {}
	audiences = {}
	dois = []
	
	for line in file:
		line = line.rstrip().lstrip()
		results = string.split(line, ';')
		
		doi = results[0]
		if len(results) > 1:
			audience = results[1]
			audiences[doi] = audience
		if len(results) > 2:
			notes[doi] = results[2]
			
		dois.append(doi)
	
	return (dois, audiences, notes)

def render_dois_to_file(dois, template_file, file=None, **kw):
	"""
	Render the list of dois passed as the first parameter using the supplied template and save it
	to the given filename.
	If dois is a string, it is interpreted as a path to a file containing a list of dois and possibly associated notes
	"""
	
	if isinstance(dois, basestring):
		(doi_list, audience, notes) = load_dois_from_file(dois)
		results = render_dois(doi_list, template_file, notes=notes, audience=audience, **kw)
	else:
		results = render_dois(dois, template_file, **kw)
	
	if file:
		outfile = open(file, "w")
	else:
		outfile = tempfile.NamedTemporaryFile(mode="w", suffix=".html", delete=False)
		file = outfile.name
	
	outfile.write(results.encode('utf8'))
	outfile.close()
	
	if "show" in kw and kw["show"]:
		cmd = "open '%s'" % file
		os.system(cmd)