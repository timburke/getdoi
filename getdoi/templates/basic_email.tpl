##A template for sending a journal update email
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<h1>Journal Update</h1>
% for journal in data.values():

	<h2>${journal[0]["journal"]["title"]}</h2>

	% for article in journal:
		<span style="font-weight:bold;">${article["article"]["title"]}</span><br />
		<span style="font-style:italic;">${article["article"]["authors"]}</span><br />
		<a href="${article["article"]["url"]}">Link</a>
		
		% if "notes" in article["article"]:
		<p style="color:green;">${article["article"]["notes"]}</p>
		% endif
		
		<p>${article["article"]["abstract"]}</p>
		
		% if "audience" in article["article"]:
		<p style="color:red;">${article["article"]["audience"]}</p>
		% endif
	% endfor
	
% endfor

<p style="font-size:80%">Generated automatically by <a href="https://bitbucket.org/timburke/getdoi/overview">getdoi</a>.</p>
</body>
</html>