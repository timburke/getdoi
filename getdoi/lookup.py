#!/usr/bin/python
#A python module to get metadata information from a DOI

import urllib2
from xml.dom import minidom
from memoize import memoized

def get_text(node):
	return " ".join(t.nodeValue for t in node.childNodes if t.nodeType == t.TEXT_NODE)

def get_child_value(parent, tag):
	children = parent.getElementsByTagName(tag)
	
	if len(children) > 0:
		node = parent.getElementsByTagName(tag)[0]
		return get_text(node)
	
	return None
	
def build_date(month, day, year):
	if month is not None and day is not None and year is not None:
		return "%s/%s/%s" % (month, day, year)
	elif month is not None and year is not None:
		return "%s/%s" % (month, year)
	elif year is not None:
		return year
	else:
		return "unknown date"

def get_journaldata(dom):
	journal = dom.getElementsByTagName('journal_issue')[0]
	journalmeta = dom.getElementsByTagName('journal_metadata')[0]
	
	title = get_child_value(journalmeta, 'full_title')
	volume = get_child_value(journal, 'volume')
	issue = get_child_value(journal, 'issue')
	month = get_child_value(journal, 'month')
	day = get_child_value(journal, 'day')
	year = get_child_value(journal, 'year')
	
	issue_name = "%s %s:%s (%s)" % (title, volume, issue, build_date(month, day, year))
	issue_subtitle = "Volume %s, Issue %s (published %s)" % (volume, issue, build_date(month, day, year))
	
	return {"title": title, 'issue-full-name': issue_name, 'issue-subtitle': issue_subtitle}

def build_authorlist(article):
	authors = article.getElementsByTagName('person_name')
		
	alist = []
	
	for author in authors:
		last = get_child_value(author, 'surname')
		first = get_child_value(author, 'given_name')
		
		name = "%s, %s." % (last, first[0])
		alist.append(name)
	
	#now format the author list accordingly
	conc = ""
	
	if len(alist) == 1:
		conc = alist[0]
	elif len(alist) == 2:
		conc = "%s and %s" % (alist[0], alist[1])
	elif len(alist) > 5:
		conc = "%s et al" % alist[0]
	else:
		#make the complete list
		for i in xrange(0, len(alist)):
			if i == 0:
				conc = alist[i]
			elif i == len(alist)-1:
				conc += " and " + alist[i]
			else:
				conc += ", " + alist[i]
				
	return conc
			
def get_articledata(dom):
	article = dom.getElementsByTagName('journal_article')[0]
	
	title = get_child_value(article, 'title')
	url = get_child_value(article, 'resource')
	authors = build_authorlist(article)
	
	return {"title": title, "url": url, "authors": authors}

@memoized
def get_metadata(doi):
	"""
	Get the metadata (title, etc.) associated with this DOI, which must be passed in as a string.
	"""
	request = "http://www.crossref.org/openurl/?id=doi:%s&noredirect=true&pid=timburke@stanford.edu&format=unixref" % doi
	
	try:
		url = urllib2.urlopen(request)
	except urllib2.URLError:
		return {"error": "URL Error"}
	
	#now parse the xml data and extract the relevant information
	dom = minidom.parse(url)
	
	metadata = {}
	
	metadata["journal"] = get_journaldata(dom)
	metadata["article"] = get_articledata(dom)
		
	return metadata